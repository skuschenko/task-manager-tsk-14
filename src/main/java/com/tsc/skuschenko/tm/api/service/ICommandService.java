package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
